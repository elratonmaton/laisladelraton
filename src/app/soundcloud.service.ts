import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SoundcloudService {

  SC: any;
  top: any[];
  player;
  constructor() {
    this.SC = require('soundcloud');

    this.SC.initialize({
      client_id: 'a281614d7f34dc30b665dfcaa3ed7505'
    });

    // this.SC.connect();

  }

  getTop() {
    return this.SC.get('/tracks?tag=random');
  }

  stream(id) {
    return this.SC.stream('/tracks/' + id);
  }
}
