import { Component, OnInit, Input } from '@angular/core';
import { SoundcloudService } from '../soundcloud.service';

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.css']
})
export class PlayerComponent implements OnInit {
  @Input() idSong: number;
  isPlaying: boolean;
  repeat_all: boolean;
  repeat_all_active: boolean;
  repeat_one_active: boolean;
  player;

  constructor(
    private sc: SoundcloudService
  ) { }

  ngOnInit() {
    this.isPlaying = false;
    this.repeat_all = true;
    this.repeat_all_active = false;
    this.repeat_one_active = false;
  }

  parseMilliseconds(mm) {
    const minutes = Math.floor(mm / 60000);
    const seconds = ((mm % 60000) / 1000).toFixed(0);
    return minutes + ':' + (+seconds < 10 ? '0' : '') + seconds;
  }

  playToggle() {
    if (this.player) {
      if (this.isPlaying) {
        console.log(this.isPlaying);
        this.player.pause();
      } else {
        this.player.play();
      }
    }
    this.isPlaying = !this.isPlaying;
  }

  repeatClick() {
    if (this.repeat_all) {
      this.repeat_all_active = true;
      this.repeat_all = false;
      return;
    }

    if (this.repeat_all_active) {
      this.repeat_one_active = true;
      this.repeat_all_active = false;
      return;
    }

    if (this.repeat_one_active) {
      this.repeat_one_active = false;
      this.repeat_all = true;
      return;
    }
  }

  checkID() {
    if (this.idSong === 0) {
      return false;
    } else {
      this.playSong(this.idSong);
    }
  }

  playSong(id) {
    if (this.player) {
      this.player.pause();
    }
    this.sc.stream(id).then(
      (play) => {
        this.player = play;
        this.player.play();
        this.isPlaying = true;
    });
  }

}
