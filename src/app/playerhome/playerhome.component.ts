import { Component, OnInit } from '@angular/core';
import { SoundcloudService } from '../soundcloud.service';

@Component({
  selector: 'app-playerhome',
  templateUrl: './playerhome.component.html',
  styleUrls: ['./playerhome.component.css']
})
export class PlayerhomeComponent implements OnInit {
  search: string;
  genre_select: string;
  progressMode: string;

  isPlaying: boolean;
  repeat_all: boolean;
  repeat_all_active: boolean;
  repeat_one_active: boolean;
  player;

  playerTitle: string;
  playerArtist: string;
  playerCover: string;

  top;

  constructor(
    private sc: SoundcloudService,
  ) { }

  ngOnInit() {
    this.search = 'Search songs...';
    this.genre_select = 'all';

    this.isPlaying = false;
    this.repeat_all = true;
    this.repeat_all_active = false;
    this.repeat_one_active = false;
    this.progressMode = 'determinate';
    this.playerCover = 'assets/img/no-album.png';

    this.sc.getTop().then((top) => {
      this.top = top;
      this.top = this.top.filter( tt => tt.artwork_url !== null);
      let indx = 0;
      this.top = this.top.filter( (tt) => {
        indx = indx + 1;
        if (indx < 7) {
          return tt;
        }
      });
    });
  }

  parseMilliseconds(mm) {
    const minutes = Math.floor(mm / 60000);
    const seconds = ((mm % 60000) / 1000).toFixed(0);
    return minutes + ':' + (+seconds < 10 ? '0' : '') + seconds;
  }

  playToggle() {
    if (this.player) {
      if (this.isPlaying) {
        console.log(this.isPlaying);
        this.player.pause();
      } else {
        this.player.play();
      }
    }
    this.isPlaying = !this.isPlaying;
  }

  repeatClick() {
    if (this.repeat_all) {
      this.repeat_all_active = true;
      this.repeat_all = false;
      return;
    }

    if (this.repeat_all_active) {
      this.repeat_one_active = true;
      this.repeat_all_active = false;
      return;
    }

    if (this.repeat_one_active) {
      this.repeat_one_active = false;
      this.repeat_all = true;
      return;
    }
  }

  playSong(tt) {
    this.playerTitle = this.parseTitle(tt.title);
    this.playerArtist = this.parseArtist(tt.title);
    this.playerCover = tt.artwork_url;
    if (this.player) {
      this.player.pause();
    }
    this.sc.stream(tt.id).then(
      (play) => {
        this.player = play;
        this.player.play();
        this.isPlaying = true;
    });
  }

  parseTitle(s: string) {
    return s.split('-')[0];
  }

  parseArtist(s: string) {
    return s.split('-')[1];
  }

  searchClick() {
    if (this.search === 'Search songs...') {
      this.search = '';
    }
  }
  searchSubmit() {
    console.log(this.search);
  }
  verifySearch() {
    if (this.search === '') {
      this.search = 'Search songs...';
    }
  }

  currentProgress() {
    if (this.player) {
      const a = ((this.player.currentTime() * 100) / this.player.getDuration()).toFixed(2);
      return a;
    } else {
      return 0;
    }
  }

}
